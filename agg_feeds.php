<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once "config.php";
include_once "feeds.php";
include_once "html_feed.php";

$feed = $_GET['feed'];

switch ($feed) {
    case 'notocn':
        $giggs_feed = new GiggsFeed('admun', $password, "http://www.giggs.hk/index.php/mn-avprog/mn-notocn",
            "不上頻道", '/tmp/giggs_feeds_cookies', 'http://www.giggs.hk/index.php/mn-member/mn-member-login?task=user.login');
        $giggs_feed->generate();
        break;
    case 'gotocn':
        $giggs_feed = new GiggsFeed('admun', $password, "http://www.giggs.hk/index.php/mn-avprog/mn-gotocn",
            "北上頻道", '/tmp/giggs_feeds_cookies', 'http://www.giggs.hk/index.php/mn-member/mn-member-login?task=user.login');
        $giggs_feed->generate();
        break;
    case 'bookfilm':
        $giggs_feed = new GiggsFeed('admun', $password, "http://www.giggs.hk/index.php/mn-avprog/mn-bookfilm",
            "細味卷影", '/tmp/giggs_feeds_cookies', 'http://www.giggs.hk/index.php/mn-member/mn-member-login?task=user.login');
        $giggs_feed->generate();
        break;
    case 'md':
        $raga_feed = new RagazineFeed('admun', $password, "http://www.ragazine.com.hk/index.php/mn-avprog/mn-md",
            "網台大典", '', '');
        $raga_feed->generate();
        break;
    case 'cinema':
        $raga_feed = new RagazineFeed('admun', $password, "http://www.ragazine.com.hk/index.php/mn-avprog/mn-backstreetcinema",
            "後巷電影院", '', '');
        $raga_feed->generate();
        break;
    case 'shop':
        $raga_feed = new RagazineFeed('admun', $password, "http://ragazine.com.hk/index.php/mn-avprog/mn-shopperlife",
            "後香港小店員網上日記", '', '');
        $raga_feed->generate();
        break;
    case 'jianman':
        $html_feed = new HTMLFeed($jianman_url, $jianman_name);
        $html_feed->generate();
        break;
    default:
        echo "\n\nmissing feed name\n\n\n";
        exit;
}

?>
