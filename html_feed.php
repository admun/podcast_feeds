<?php

class HTMLFeed {

    private $feed_header;
    private $feed_footer = "        </channel>

    </rss>";
    private $url;

    function __construct($feed, $title) {
        $this->feed_header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <rss xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\" version=\"2.0\">

        <channel>

            <title>" . $title . "</title>
            <description>Some podcast just does not have RSS feed, so here I generate the feed from their HTML for myself</description>
";
        $this->url = $feed;
    }

    private function fetch_html($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        return curl_exec($ch);
    }

    function generate() {
        echo $this->feed_header;
        $this->process($this->fetch_html($this->url));
        echo $this->feed_footer;
    }

    private function process($feedData) {
        error_reporting(0);
        $dom = new DOMDocument;
        $dom->loadHTML($feedData);
        $items = $dom->getElementsByTagName('td');
        foreach ($items as $item) {
            $text = explode("\n", $item->textContent);
            if (isset($item->getElementsByTagName("a")->item(0)->attributes) && isset($text[2])) {
                $url = $item->getElementsByTagName("a")->item(0)->attributes->item(0)->value;
                $date = str_replace('日', '', str_replace('月', '-', str_replace('年', '-', $text[0]))) . 'T00:00:00+08:00';
                print "<item>\n";
                print "<title>$text[0] - $text[2]</title>\n";
                print "<link>$url</link>\n";
                print "<guid>$url</guid>\n";
                print "<enclosure url=\"$url\"/>\n";
                print "<pubDate>$date</pubDate>\n";
                print "</item>\n";

            }
        }
   }
}