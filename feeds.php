<?php

include "feedbase.php";

class RagazineFeed extends Feed {
    protected function process($feedData) {
        $xml = new SimpleXMLElement($feedData);
        foreach ($xml->entry as $element) {
            $url = $element->id;
            preg_match_all('|audio_mp3=\[(.*)\]|U', $element->summary, $matches);
            if (!empty($matches[1])) {
                foreach ($matches[1] as $mp3) {
                    $filenames_part = explode('/', strip_tags($mp3));
                    $filename = array_pop($filenames_part);
                    $name = $element->title . " - " . $filename;
		    if (substr($mp3, 0, 4) != "http") {
			$url = "http://acm1450.com/ragazine/" . str_replace("/images/", "",  $mp3);
		    } else {
			$url = $mp3;
	            }
                    print "<item>\n";
                    print "<title>{$name}</title>\n";
                    print "<link>{$url}</link>\n";
                    print "<guid>$url</guid>\n";
                    print "<enclosure url=\"". $url . "\"/>\n";
                    print "<pubDate>{$element->updated}</pubDate>\n";
                    print "</item>\n";
                }
            }
        }
    }
}

class GiggsFeed extends Feed {
    protected function process($feedData) {
        $xml = new SimpleXMLElement($feedData);
        foreach ($xml->entry as $element) {
            $dom = new DOMDocument;
            $dom->loadHTML(mb_convert_encoding($element->content, 'HTML-ENTITIES', 'UTF-8'));
            foreach($dom->getElementsByTagName('a') as $link) {

                $name = $link->nodeValue;
                $url = $link->getAttribute("href");
                if (substr($url, 0, 4) != "http") {
                    $url = "http://www.ragazine.com.hk/$url";
                }
                $path_info = pathinfo($url);
                if (!empty($path_info['extension']) && in_array($path_info['extension'], array("m4a", "mp3")) && $name != '後備連結' && !empty($name)) {
                    print "<item>\n";
                    print "<title>{$element->title} - {$name}</title>\n";
                    print "<link>{$element->id}</link>\n";
                    print "<guid>$url</guid>\n";
                    print "<enclosure url=\"$url\"/>\n";
                    print "<pubDate>{$element->updated}</pubDate>\n";
                    print "</item>\n";
                }
            }

        }
    }
}
