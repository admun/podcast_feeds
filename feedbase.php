<?php

interface iFeed {
    function generate();
}

abstract class Feed implements iFeed {
    private $feed;
    private $cookie_filepath;
    private $feed_header;
    private $feed_footer = "        </channel>

    </rss>";

    function __construct($username, $password, $feed, $title, $cookie_filepath, $auth_cookie_url) {
        $this->cookie_filepath = $cookie_filepath;
	if (!empty($auth_cookie_url)) {
        	$this->generate_auth_cookie($auth_cookie_url, $cookie_filepath, $username, $password);
	}
        $this->feed = $feed;
        $this->feed_header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <rss xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\" version=\"2.0\">

        <channel>

            <title>" . $title . "</title>
            <description>Ragazine's mobile site podcast listening experience sucks and they do not provide podcast RSS feeds, so here I generate the feed for myself</description>
";
    }

    private function setup_curl($feed, $cookie_filepath) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	if (!empty($cookie_filepath)) {
        	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_filepath);
        	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_filepath);
	}
        curl_setopt($ch, CURLOPT_URL, $feed);

        return $ch;
    }

    private function generate_auth_cookie($url, $cookie_filepath, $username, $password) {
        $fields = array(
            'username' => $username,
            'password' => $password
        );

        $fields_string = '';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_filepath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_filepath);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_exec($ch);
        curl_close($ch);
    }

    private function get_feed($feed, $cookie_filepath) {
        $ch = $this->setup_curl($feed."?format=feed&type=atom", $cookie_filepath);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function generate() {
        echo $this->feed_header;
        $feedData = $this->get_feed($this->feed, $this->cookie_filepath);
        $this->process($feedData);
        echo $this->feed_footer;
    }

    abstract protected function process($feedData);
}
